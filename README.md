php-Gradient
============

A php library for creating and calculating gradients with color stops


Features
--------

- Add color stops
- Calculate the color on the gradient at a certain percentage


Usage
-----

Load the library and initialize:
require("gradient.php");
$gradient = new Gradient();


Add color stops:
$gradient->add(percentage, color);


Get color at e.g. 15%:
$gradient->at(15);


Sample
------

Comes with a sample consisting of index.php, style.css and sample.php.
It compares a CSS3 gradient with 1000 small boxes making a gradient.