<?php require("sample.php"); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

    <head>

        <title>
            php-Gradient
        </title>

        <link rel="stylesheet" href="style.css" media="screen" />

        <meta charset="utf-8" />

        <meta http-equiv="refresh" content="100"> 
                
        <?php
        $rgb = $gradient->at(percentage_day());
        $r = $rgb['r'];
        $g = $rgb['g'];
        $b = $rgb['b'];
        ?>
        
        <style>
            html {
                background: rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>);
            }
        </style>
        
    </head>

    <body>

        <div id="body-container">
            <h1>php-Gradient</h1>

            <hr />
            
            Gradient sample (CSS)
            <div class="sample sky"><br /></div>

            <br />
            
            <?php $number = 1000; // style.css must also be updated (.box width) by same factor ?>
            
            Gradient made by <?php echo $number; ?> small boxes
            <div>
            <?php for($i=0; $i<=$number; $i++): 
            $rgb_i = $gradient->at($i/($number/100));
            $r_i = $rgb_i['r'];
            $g_i = $rgb_i['g'];
            $b_i = $rgb_i['b'];
            ?>
            <span class="box" style="background: rgb(<?php echo $r_i; ?>, <?php echo $g_i; ?>, <?php echo $b_i; ?>);">&nbsp;</span>
            <?php endfor; ?>
            </div>
            
            <br />
             
            <hr />
            Background changes according to the time of day.
            <br />
            Percentage of day spend: 
            <?php echo percentage_day(); ?> % 
            <br />
            Background color: 
            rgb(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>)
            
            
        </div>
        
    </body>

</html>