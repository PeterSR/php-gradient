<?php

// Percentage of day spend
function percentage_day()
{
    $s_day = mktime(24, 0, 0) - mktime(0, 0, 0); // Seconds on a day = 86400 (60*60*24)
    $s_spend = time() - mktime(0, 0, 0); // Seconds already spend

    $percentage_spend = (($s_spend) / ($s_day)) * 100;

    //return ((date("s")) / (60)) * 100; // Test day -- 1 minute
    
    return $percentage_spend;
}

// Load Gradient library
require_once('gradient.php');

// Apply following gradient (from css)
// #ccdbff 0%,#ff8912 3%,#ffb46b 9%,#ffece0 51%,#ffb969 88%,#ff8d0b 96%,#ccdbff 100%;

$gradient->add(0, "#ccdbff");
$gradient->add(3, "#ff8912");
$gradient->add(9, "#ffb46b");
$gradient->add(51, "#ffece0");
$gradient->add(88, "#ffb969");
$gradient->add(96, "#ff8d0b");
$gradient->add(100, "#ccdbff");

// Example
$rgb = $gradient->at(15); // RGB color from gradient at 15 %

?>
