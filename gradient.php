<?php

class Gradient
{

    /**
     * @var Array 
     */
    private $gradient = Array();

    public function __construct()
    {
        
    }

    public function __destruct()
    {
        
    }

    /**
     * Returns the gradiens array
     */
    public function get()
    {
        return $this->gradient;
    }

    /**
     * Sort the gradient array according to the color stop percentages
     */
    public function sort()
    {
        usort($this->gradient, create_function('$a,$b', 'return ($a[\'pct\'] > $b[\'pct\']) ? 1 : -1;'));
    }

    /**
     * Recognize color from string
     * @param string
     * @return string
     */
    public function color_recognizer($color)
    {
        // Recognize color
        if (preg_match("/^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/", $color)) // Hex
        {
            return "hex";
        }
        else if (preg_match("/^rgb\(([01]?\d\d?|2[0-4]\d|25[0-5])\,( )*([01]?\d\d?|2[0-4]\d|25[0-5])\,( )*([01]?\d\d?|2[0-4]\d|25[0-5])\)$/", $color)) // RGB
        {
            return "rgb";
        }
        else // Color not recognized
            return false;
    }

    /**
     * Blend between two color stops at a certain percentage
     * @param $at 
     * @param $from Color stop #1 -- array("pct" => (int), "rgb" => array("r" => (int), "g" => (int), "b" => (int)))
     * @param $to Color stop #2 -- array("pct" => (int), "rgb" => array("r" => (int), "g" => (int), "b" => (int)))
     */
    public function color_fade($at, $from, $to)
    {
        $from_pct = $from['pct'];
        $to_pct = $to['pct'];

        $at_pct = (($at - $from_pct) / ($to_pct - $from_pct));

        $from_color = $from['rgb'];
        $to_color = $to['rgb'];

        $rgb = Array(
            "r" => round($from['rgb']['r'] + ($to_color['r'] - $from_color['r']) * $at_pct),
            "g" => round($from['rgb']['g'] + ($to_color['g'] - $from_color['g']) * $at_pct),
            "b" => round($from['rgb']['b'] + ($to_color['b'] - $from_color['b']) * $at_pct)
        );

        return $rgb;
    }

    /**
     * Add color stop
     * @param $percent integer - range(0, 100)
     * @param $color string - #RRGGBB, rgb(r,g,b)
     */
    public function add($percentage, $color)
    {
        $rgb = Array("r" => null, "g" => null, "b" => null);

        // Recognize color from string
        switch ($this->color_recognizer($color))
        {
            case "hex": // In case of a hex color
                if (substr($color, 0, 1) == "#")
                    $color = substr($color, 1);

                switch (strlen($color))
                {
                    case 3:
                        $rgb["r"] = hexdec(substr($color, 0, 1) . substr($color, 0, 1));
                        $rgb["g"] = hexdec(substr($color, 1, 1) . substr($color, 1, 1));
                        $rgb["b"] = hexdec(substr($color, 2, 1) . substr($color, 2, 1));
                        break;
                    case 6:
                        $rgb["r"] = hexdec(substr($color, 0, 2));
                        $rgb["g"] = hexdec(substr($color, 2, 2));
                        $rgb["b"] = hexdec(substr($color, 4, 2));
                        break;
                }
                break;

            case "rgb": // In case of a rgb color
                $color = str_replace(" ", "", $color);

                if (substr($color, 0, 3) == "rgb")
                    $color = substr($color, 3);

                if (substr($color, 0, 1) == "(")
                    $color = substr($color, 1);

                if (substr($color, strlen($color) - 1, 1) == ")")
                    $color = substr($color, 0, strlen($color) - 1);

                $color_array = explode(",", $color);

                $rgb["r"] = $color_array[0];
                $rgb["g"] = $color_array[1];
                $rgb["b"] = $color_array[2];
                break;

            default: // Color not recognized
                return false;
                break;
        }

        $this->gradient[] = array("pct" => $percentage, "rgb" => $rgb);

        return true;
    }

    /**
     * Find color at percentage
     * @param $percentage
     * @param $fade Determine whether the color fades or just chooses the nearest stop
     */
    public function at($percentage, $fade = true)
    {
        $rgb = Array("r" => null, "g" => null, "b" => null);

        $this->sort();

        // Iterate througe color stops
        for ($i = 0; $i < count($this->gradient); $i++)
        {
            // Current percentage ($i) and next percentage ($i+1)
            $current_pct = $this->gradient[$i]['pct'];
            $next_pct = $this->gradient[$i + 1]['pct'];

            // If percentage is a color stop
            if ($current_pct == $percentage)
            {
                $rgb['r'] = $this->gradient[$i]['rgb']['r'];
                $rgb['g'] = $this->gradient[$i]['rgb']['g'];
                $rgb['b'] = $this->gradient[$i]['rgb']['b'];
                break;
            }

            // If percentage is between two color stops
            if ((($current_pct < $percentage) && ($percentage < $next_pct)))
            {
                if (!$fade)
                {
                    if (($percentage - $current_pct) < ($next_pct - $percentage))
                        $nearest = $i;
                    else
                        $nearest = $i + 1;

                    $rgb = $this->gradient[$nearest]['rgb'];
                    break;
                }
                else
                {
                    $rgb = $this->color_fade($percentage, $this->gradient[$i], $this->gradient[$i + 1]);
                    break;
                }
            }
            else
                continue;
        }

        return $rgb;
    }

}

$gradient = new Gradient();
?>
